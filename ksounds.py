import os
import os.path
import numpy as np
from nextcord import File
import pandas as pd
from nextcord import Color
from nextcord import Embed
from nextcord.ext import commands
from unidecode import unidecode

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'center')
pd.set_option('display.precision', 2)


class Kaamelott_perso():
    """

    """
    __slots__ = ('character', 'episode', 'file', 'line', 'avatar')

    def __init__(self):
        self.character = ""
        self.episode = ""
        self.line = ""
        self.avatar = ""

    def get_avatar_from_name(self, name):
        path = "kaamelott_avatar/"
        valid_images = [".jpg", ".gif", ".png", ".tga"]
        for f in os.listdir(path):
            ext = os.path.splitext(f)[1]
            if ext.lower() not in valid_images:
                continue
            if name in f.lower():
                return os.path.join(path, f)


class Kaamelott_embed():
    """

    """

    def __init__(self):
        self.embed = Embed()

    async def kembed(self, ctx: commands.Context, kperso: Kaamelott_perso()):
        """

        :param ctx:
        :type ctx:
        :param kperso:
        :type kperso:
        :return:
        :rtype:
        """
        self.embed.set_author(name=ctx.author.name, icon_url=ctx.author.avatar)
        file = File(kperso.avatar, filename=kperso.character + ".jpg")
        self.embed.set_thumbnail(url=f"attachment://{kperso.character}.jpg")
        self.embed.title = kperso.character.upper()
        self.embed.add_field(name='Episode', value=kperso.episode, inline=True)
        self.embed.add_field(name='Phrase', value=kperso.line.replace("_", " ").replace("mp3", "").replace("-", " "),
                             inline=False)
        self.embed.set_footer(text="Information requested by: {}".format(ctx.author.display_name))
        self.embed.color = Color.dark_blue()  # This can be any color, but I chose a nice dark red tint
        await ctx.send(file=file, embed=self.embed)

    async def multilines_embed(self, ctx: commands.Context, lines: list):

        if (len(lines) > 1):
            self.embed.title = "Plusieurs phrases trouvees"
        else:
            self.embed.title = "Phrase trouvees"
        for idx, line in enumerate(lines):
            self.embed.add_field(name=f"Phrases : {idx + 1}",
                                 value=line.replace("_", " ").replace("mp3", "").replace("-", " "), inline=False)
            self.embed.color = Color.blurple()  # This can be any color, but I chose a nice dark red tint
        await ctx.send(embed=self.embed)


class CharacterNotFound(FileNotFoundError):
    pass


class Ksounds():
    """

    """

    def __init__(self):
        self._sounds = pd.read_json('kaamelott_sounds.json')
        self._sounds["character"] = self._sounds["character"].astype(str).str.lower().apply(unidecode)
        self.kperso = Kaamelott_perso()
        # print(self._sounds)

    def get_all_characters(self):
        """

        :param characters:
        :type characters:
        :return:
        :rtype:
        """
        return '\n'.join(pd.unique(self._sounds['character']))

    def get_random_from_characters(self, characters: str):
        """
        :param characters:
        :type characters:
        :return:
        :rtype:
        """
        print(characters)
        if characters in self.get_all_characters():
            return self._sounds.loc[self._sounds['character'] == characters]['file'].sample().item()
        else:
            return None

    def get_line_from_character(self, character: str, line: str):
        print(f" in get_line_from_character {character} : {line}")

        if character in self.get_all_characters():
            # print(self._sounds.loc[self._sounds['character'] == character]['file'])

            df = self._sounds.loc[self._sounds['character'] == character]['file']
            print("------------------------------")
            # print(df)
            if len(df.index) > 0:
                print(df[df.str.contains(line)].values[0])
                return df[df.str.contains(line)].values[0]
            else:
                return None
        else:
            return None

    def get_lines_from_character(self, character: str, line: tuple):
        print(f" in get_line_from_character =  {character} : {line}")
        print(len(line))
        print(character in self.get_all_characters())
        if not character in self.get_all_characters():
            return CharacterNotFound()
        else:
            if len(line) > 0:
                # print(self._sounds.loc[self._sounds['character'] == character]['file'])

                df = self._sounds.loc[self._sounds['character'] == character]
                df = df.assign(title=df['title'].astype(str).str.lower().apply(unidecode))
                mask = df['title'].apply(lambda sentence: all(word in sentence for word in line))
                df = (df[mask])

                if len(df.index) > 0:
                    return df['file'].values
                else:
                    return None
            elif len(line) == 0:
                print(self._sounds.loc[self._sounds['character'] == character]['file'].values)
                return self._sounds.loc[self._sounds['character'] == character]['file'].values
            else:
                return None

    def get_episode_from_line(self, line):
        return self._sounds.loc[self._sounds['file'] == line]['episode'].values[0]
