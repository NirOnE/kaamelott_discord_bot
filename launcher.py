from bot import KaamelottBot

def main():
    bot = KaamelottBot()
    bot.run()


if __name__ == "__main__":
    main()