import asyncio
import json
from typing import Optional
import nextcord
from numpy import character

import ksounds
from asyncio import sleep
from nextcord.ext import commands
from nextcord import message
from nextcord import FFmpegPCMAudio
from nextcord.utils import get
from nextcord import VoiceChannel

class Kplaybot(commands.Cog, name="Kaamelott sounds"):
    """Plays Kaamelott sounds in discord"""

    def __init__(self, bot: commands.Bot):
        """
        init method of the class, call the Client init
        :param args:
        :type args:
        """
        # super().__init__(*args, **kwargs)
        self.bot = bot  # defining bot as global var in class
        self.queue = {}
        self.sounds = ksounds.Ksounds()


    @commands.command()
    async def join(self, ctx: commands.Context):
        if ctx.author.voice is None:
            return await ctx.send("You are not connected to a voice channel, please connect to the channel you want the bot to join.")

        if ctx.voice_client is not None:
            await ctx.voice_client.disconnect()

        await ctx.author.voice.channel.connect()

    @commands.command(name="characters")
    async def characters(self, ctx: commands.Context):
        """
        Affiche les differents characters de Kaamelott
        :param ctx:
        :return:
        :type ctx:
        :rtype:
        """
        embed = nextcord.Embed(title='Kaamelott characters')
        embed.add_field(name='Kaamelott', value=self.sounds.get_all_characters())
        await ctx.send(embed=embed)

    @commands.command(name="search", aliases=['s'])
    async def search(self, ctx: commands.Context, character, *line_part: Optional[str]):
        await ctx.send(f'{character} arguments from play: {line_part}')
        kembed = ksounds.Kaamelott_embed()
        lines = self.sounds.get_lines_from_character(character, line_part)
        print(f"search {lines}")
        print(f"in search number of lines : {len(lines)}")
        await kembed.multilines_embed(ctx, lines)

    @commands.command(name="play", aliases=['p'])
    async def play(self, ctx: commands.Context, character: str, *line: Optional[str] ):
        """
        Get the bot's current websocket latency.
        :param ctx:
        :type ctx:
        :return:
        :rtype:
        """
        kperso = ksounds.Kaamelott_perso()
        kembed = ksounds.Kaamelott_embed()
        channel = get(ctx.guild.voice_channels, name="testing")
        voice = get(self.bot.voice_clients, guild=ctx.guild)
        kperso.character = character.lower()
        kperso.avatar = kperso.get_avatar_from_name(character)


        if ctx.author.voice is None:
            return await ctx.send("You are not connected to a voice channel, please connect to the channel you want the bot to join.")

        voice_channel = ctx.message.author.voice.channel
        channel = None

        if voice_channel is not None:
            channel = voice_channel.name

            if voice is None:
                await voice_channel.connect()

            if line is None:
                kperso.episode = "TBD"
                kperso.line = "TBD"
            elif line == 'random':
                kperso.line = "random"
            else:
                lines = self.sounds.get_lines_from_character(character, line)
                print(f"in play ===== {lines} ****** {len(lines)}")
                if len(lines) == 1:
                    kperso.line = lines[0]
                    kperso.episode = self.sounds.get_episode_from_line(kperso.line)
                else:
                    await kembed.multilines_embed(ctx, lines)
                    return

            # await ctx.send('{} arguments from play: {}'.format(len(args), ', '.join(args)))

            channel = get(ctx.guild.voice_channels, name="testing")
            voice = get(self.bot.voice_clients, guild=ctx.guild)

            if kperso.line == "TBD":
                await ctx.send(f"le character ou la phrase de {character} n'existe pas")
            else:
                if kperso.line == "random":
                    kperso.line = self.sounds.get_random_from_characters(character)
                    kperso.episode = self.sounds.get_episode_from_line(kperso.line)
                voice.play(FFmpegPCMAudio(source="sounds/"+str(kperso.line).__str__()))
                await kembed.kembed(ctx, kperso)


        else:
            await ctx.send(str(ctx.author.name) + "is not in a channel.")
        # Delete command after the audio is done playing.
        # await ctx.message.delete()





    @commands.command(name="ping")
    async def ping(self, ctx: commands.Context):
        """
        Get the bot's current websocket latency.
        :param ctx:
        :type ctx:
        :return:
        :rtype:
        """
        await ctx.send(f"Pong! {round(self.bot.latency * 1000)}ms")  # It's now self.bot.latency

    @commands.command(name="info")
    async def info(self, ctx: commands.Context):
        """
        Get the bot's current websocket latency.
        :param ctx:
        :type ctx:
        :return:
        :rtype:
        """
        await ctx.send(ctx.guild)  # It's now self.bot.latency
        await ctx.send(ctx.author)  # It's now self.bot.latency
        await ctx.send(ctx.message.id)  # It's now self.bot.latency

    @commands.Cog.listener()
    async def on_message(self, message: message):
        """
        Bot reponse to message
        :param message:
        :type message:
        :return:
        :rtype:
        """

        if message.author.bot:
            return

        if "hello" in message.content.lower():  # A simple example, don't want to get too complex
            await message.channel.send("Hi!")

        if 'seb' in message.content.lower():
            await message.add_reaction('\U0001F410')

def setup(bot: commands.Bot):
    """

    :param bot: bot object
    :type bot: commands.Bot
    :return:
    :rtype:
    """
    bot.add_cog(Kplaybot(bot))
