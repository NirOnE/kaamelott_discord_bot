import os
from pathlib import Path

import nextcord
from dotenv import load_dotenv
from nextcord import Embed
from nextcord.ext import commands

bot = commands.Bot(command_prefix="!")

class KaamelottBot(commands.Bot):
    def __init__(self):
        self._cogs = [p.stem for p in Path(".").glob("./cogs/*.py")]
        super().__init__(command_prefix=self.prefix, case_insensitive=True, intents=nextcord.Intents.all())
        self.bot = bot

    def setup(self):
        print("running setup ... ")
        for cog in self._cogs:
            print(cog)
            self.load_extension(f"cogs.{cog}")
            print(f" Loaded `{cog}` cog.")

        print("Setup complete.")

    def run(self):
        self.setup()
        load_dotenv()
        TOKEN = os.getenv('DISCORD_TOKEN')
        print("Running bot...")
        super().run(TOKEN, reconnect=True)

    async def shutdown(self):
        print("Closing connection to Discord...")
        await super().close()

    async def close(self):
        print("Closing on keyboard interrupt...")
        await self.shutdown()

    async def on_connect(self):
        print(f" Connected to Discord (latency: {self.latency * 1000:,.0f} ms).")

    async def on_resumed(self):
        print("Bot resumed.")

    async def on_disconnect(self):
        print("Bot disconnected.")

    async def on_error(self, err, *args, **kwargs):
        raise

    async def on_command_error(self, ctx, exc):
        raise getattr(exc, "original", exc)

    @commands.Cog.listener()  # this is a decorator for events/listeners
    async def on_ready(self):
        self.client_id = (await self.application_info()).id
        print("KaamelottBot est ready.")

    async def prefix(self, bot, msg):
        return commands.when_mentioned_or("!")(bot, msg)

    async def process_commands(self, msg):
        ctx = await self.get_context(msg, cls=commands.Context)

        if ctx.command is not None:
            await self.invoke(ctx)

    async def on_message(self, msg):
        if not msg.author.bot:
            await self.process_commands(msg)
